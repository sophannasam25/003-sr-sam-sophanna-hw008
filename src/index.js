import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

import { Form } from "react-bootstrap";
import Tables from "./components/Tables";


ReactDOM.render(
  <React.StrictMode>
    <App />
    <Tables />
    <Form />
    
  </React.StrictMode>,
  document.getElementById("root")
);
