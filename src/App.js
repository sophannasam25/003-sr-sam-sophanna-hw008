import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Container,Row } from 'react-bootstrap';
import Form from './components/Form';
import Tables from './components/Tables';

class App extends Component {
  render() {
    return (
     <Container>
       <Row>
         <Col sm={4}>
          <Form/>
         </Col>
         <Col sm={8}>
          <Tables/>
         </Col>
       </Row>
     </Container>     
    );
  }
}

export default App;
