import React, { Component } from 'react'
import {Table} from 'react-bootstrap'

class Tables extends Component {
    render() {
        return (
            <div>
                <h1>Table Account</h1>
                <Table striped bordered hover>
    <thead>
      <tr>
        <th>#</th>
        <th>Username</th>
        <th>Email</th>
        <th>Gender</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Mavin</td>
        <td>mavinsao11@gmail.com</td>
        <td>male</td>
      </tr>
    </tbody>
  </Table>                     
            </div>
        )
     }
}

export default Tables;
