import React, { Component } from "react";
import { Navbar, Container, Button, Row } from "react-bootstrap";

class Form extends Component {
  constructor(props){
    super(props);
    this.state={
      username:"",
      email:"",
      password:"",
      emailErr:"",
      passErr:"",
      usernameErr:""
        }
  }

  onHandleEmail=(e)=>{
    console.log("e:",e.target);
    this.setState({
      [e.target.name]:e.target.value
    },()=>{
      console.log(this.state)

      if(e.target.name==="username"){
        let pattern3 = /^[\w\s-]{3,13}$/g;
          let result3 =pattern3.test(this.state.username)
          if(result3){
              this.setState({
                usernameErr:""
              })
          }else if(this.state.username===""){
                  this.setState({
                    usernameErr:"Username cannot be empty"
                  })
              }else{
                   this.setState({
                    usernameErr:"No more than 9 characters, No less than 3 characters"
            })
              }
             
            }


      if(e.target.name==="email"){
        let pattern = /^\S+@\S+\.[a-z]{3}$/g;
          let result =pattern.test(this.state.email.trim())
          if(result){
              this.setState({
                emailErr:""
              })
          }else if(this.state.email===""){
                  this.setState({
                      emailErr:"Email cannot be empty"
                  })
              }else{
                   this.setState({
                emailErr:"Email is invalid"
            })
              }
             
            }


            if(e.target.name==="password"){
              let pattern2 = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[_\w])([a-zA-Z0-9_\W]{8,})$/g;
          let result2 =pattern2.test(this.state.password)
          if(result2){
              this.setState({
                passErr:""
              })
          }else if(this.state.password===""){
                  this.setState({
                    passErr:"Password cannot be empty"
                  })
              }else{
                   this.setState({
                    passErr:"Password is invalid"
            })
              }
            }
  })
  }
  render() {
    return (
      <Container>
        {/* ************* Navbar **************** */}
        <Navbar className="Nav">
          <Navbar href="#home">
            KSHRD Student Gen 09<sup>th</sup>
          </Navbar>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            <Navbar.Text>
              Signed in as: <a href="#login">Phanna</a>
            </Navbar.Text>
          </Navbar.Collapse>
        </Navbar>
        {/* ************* Form **************** */}
        <Row>
          <Form className="col-md-4">
            <Form.Group controlId="formBasicEmail">
              <Form.Label>User Name</Form.Label>
              <Form.Control
                onChange={this.onHandleEmail}
                name="username"
                type="username"
                placeholder="Enter Username"
              />
              <Form.Text className="text-muted">
               <p style={{color:"red"}}>{this.state.usernameErr}</p>
              </Form.Text>
            </Form.Group>
        
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                onChange={this.onHandleEmail}
                name="email"
                type="email"
                placeholder="Enter email"
              />
               <Form.Text className="text-muted">
               <p style={{color:"red"}}>{this.state.emailErr}</p>
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                onChange={this.onHandleEmail}
                name="password"
                type="email"
                placeholder="Password"
              />
               <Form.Text className="text-muted">
               <p style={{color:"red"}}>{this.state.passErr}</p>
              </Form.Text>
            </Form.Group>
            <Form.Group controlId="formBasicCheckbox">
              <Form.Check type="checkbox" label="Check me out" />
            </Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>

         

          
        </Row>
      </Container>
    );
  }
}

export default Form;